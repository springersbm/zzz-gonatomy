package gonatomy

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"
)

const testMethod = "GET"
const testURL = "http://example.com/foo"

func Test_it_logs_request_details_and_error_details(t *testing.T) {
	middleWare, logger := newTestMiddleWare()
	boringHandler := func(w http.ResponseWriter, r *http.Request) {
		http.Error(w, "something failed", http.StatusInternalServerError)
	}

	req, _ := http.NewRequest(testMethod, testURL, nil)
	w := httptest.NewRecorder()

	middleWare.ServeHTTP(w, req, boringHandler)

	expectedRequestInfo := fmt.Sprintf(requestInfoTemplate, testMethod, testURL, []string{})

	if !logger.HadInfoMessage(expectedRequestInfo) {
		t.Fatalf("The logger didnt record the request info as expected, expected [%s] got [%s]", expectedRequestInfo, logger.infoMessages)
	}

	expectedResponseInfo := fmt.Sprintf(responseInfoTemplate, middleWare.appName, http.StatusInternalServerError, testURL)

	if !logger.HadErrorMessage(expectedResponseInfo) {
		t.Fatalf("The logger didnt record the error response info as expected, expected [%s], got [%s]", expectedResponseInfo, logger.errorMessages)
	}
}

func Test_it_logs_non_500_responses_as_info(t *testing.T) {
	middleWare, logger := newTestMiddleWare()
	boringHandler := func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
		fmt.Fprint(w, "Hello, world")
	}
	req, _ := http.NewRequest(testMethod, testURL, nil)
	w := httptest.NewRecorder()

	middleWare.ServeHTTP(w, req, boringHandler)
	expectedResponseInfo := fmt.Sprintf(responseInfoTemplate, middleWare.appName, http.StatusOK, testURL)

	if !logger.HadInfoMessage(expectedResponseInfo) {
		t.Fatalf("The logger didnt record the error response info as expected, expected [%s], got [%s]", expectedResponseInfo, logger.infoMessages)
	}
}

type stubLogger struct {
	Logger
	infoMessages  []string
	errorMessages []string
}

func (s *stubLogger) Info(msg string)  { s.infoMessages = append(s.infoMessages, msg) }
func (s *stubLogger) Error(msg string) { s.errorMessages = append(s.errorMessages, msg) }

func (s *stubLogger) HadInfoMessage(msg string) bool {
	return exists(msg, s.infoMessages)
}

func (s *stubLogger) HadErrorMessage(msg string) bool {
	return exists(msg, s.errorMessages)
}

func exists(needle string, haystack []string) bool {
	for _, x := range haystack {
		if x == needle {
			return true
		}
	}
	return false
}

type stubStats struct {
	lastName  string
	lastValue int64
}

func (s *stubStats) Timing(name string, value int64) error {
	s.lastName = name
	s.lastValue = value
	return nil
}
func (s *stubStats) Incr(name string, value int64) error {
	s.lastName = name
	s.lastValue = value
	return nil
}
func (s *stubStats) Absolute(name string, value int64) error {
	s.lastName = name
	s.lastValue = value
	return nil
}
func (s *stubStats) Close() error { return nil }

func newTestMiddleWare() (*MiddleWare, *stubLogger) {
	logger := &stubLogger{}
	return NewMiddleWare(&stubStats{}, logger, testServiceName), logger
}
