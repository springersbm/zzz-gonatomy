# Gonatomy - Tools for Springer App Anatomy [![GoDoc](https://godoc.org/bitbucket.org/springersbm/gonatomy?status.svg)](https://godoc.org/bitbucket.org/springersbm/gonatomy)

## Why would you use this?

- Automatic logging, timing and counts of HTTP controllers
- Loggers and metric tools provided out of the box

## Example

https://bitbucket.org/springersbm/microservice-example-go

## Building

### Checking out

You will need golang installed as directed by https://golang.org/doc/code.html.

    $ cd $GOPATH/src/bitbucket.org/springersbm
    $ git clone git@bitbucket.org:springersbm/gonatomy.git

### Tests & coverage

    $ ./build
    $ go tool cover -html=coverage.out

## Todo

- More tests!
- Might bring in config too, but that is really easy so..
- Autodocumentation