package gonatomy

import (
	"os"
	"testing"
)

func TestItReturnsEnvironmentVariablesAsMap(t *testing.T) {
	os.Setenv("foo", "bar")

	env := getEnvironmentVaribles()

	if env["foo"] != "bar" {
		t.Error("It doesn't look like the env variables were read into a map")
	}
}
