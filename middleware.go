package gonatomy

import (
	"fmt"
	"net/http"
	"time"
)

// MiddleWare encapsulates standard logging and metrics for HTTP handlers using Negroni
type MiddleWare struct {
	stats            StatsReporter
	logger           Logger
	appName          string
	timingMetricName string
}

// NewMiddleWare returns an instance of a middleware
func NewMiddleWare(stats StatsReporter, logger Logger, appName string) *MiddleWare {
	return &MiddleWare{stats, logger, appName, "timing"}
}

const requestInfoTemplate = "Request received [%s] [%s]. Accept header: %s"
const responseInfoTemplate = "Controller [%s] returning status code [%d] for [%s]"

// ServeHTTP is an implementation of github.com/codegangsta/negroni Handler interface so requests can be logged and measured
func (r *MiddleWare) ServeHTTP(rw http.ResponseWriter, req *http.Request, next http.HandlerFunc) {
	loggingResponseWriter := newStatusLoggingResponseWriter(rw)

	r.logger.Info(fmt.Sprintf(requestInfoTemplate, req.Method, req.URL, req.Header["Accept"]))
	r.stats.Incr("requests", 1)

	start := time.Now()
	next(loggingResponseWriter, req)

	r.logResponseCode(loggingResponseWriter.StatusCode, req.URL.String())
	r.incrementStatusCode(loggingResponseWriter.StatusCode)
	r.reportResponseTime(start)
}

func (r *MiddleWare) incrementStatusCode(code int) {
	r.stats.Incr(fmt.Sprintf("responsecode.%d", code), 1)
}

func (r *MiddleWare) reportResponseTime(start time.Time) {
	r.stats.Timing(r.timingMetricName, int64(time.Since(start).Nanoseconds()/int64(time.Millisecond)))
}

func (r *MiddleWare) logResponseCode(code int, url string) {
	statusMsg := fmt.Sprintf(responseInfoTemplate, r.appName, code, url)

	if code == http.StatusInternalServerError {
		r.logger.Error(statusMsg)
	} else {
		r.logger.Info(statusMsg)
	}
}
