package gonatomy

import (
	"log"
)

type stdOutTimer struct{}

func newStdOutTimer() *stdOutTimer {
	return new(stdOutTimer)
}

func (s *stdOutTimer) Timing(msg string, durationInMills int64) error {
	log.Printf("Timing [%s] took %dms\n", msg, durationInMills)
	return nil
}

func (s *stdOutTimer) Incr(stat string, count int64) error {
	log.Printf("Incrementing counter metric [%s] by %d", stat, count)
	return nil
}

func (s *stdOutTimer) Absolute(stat string, value int64) error {
	log.Printf("Absolute-valued metric [%s] sent value of [%d]", stat, value)
	return nil
}

func (s *stdOutTimer) Close() error {
	log.Printf("Closing StdOutTimer\n")
	return nil
}
