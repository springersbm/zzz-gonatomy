package gonatomy

import (
	"encoding/json"
	"github.com/robertkowalski/graylog-golang"
	"strconv"
)

const infoLevel = 1
const errorLevel = 3
const debugLevel = 0

type gelfLogger interface {
	Log(string)
}

type graylogger struct {
	gelf        gelfLogger
	serviceName string
	environment string
}

func newGrayLogger(port int, grayLogURL string, serviceName string, environment string) *graylogger {
	gelf := gelf.New(gelf.Config{
		GraylogPort:     port,
		GraylogHostname: grayLogURL,
	})
	logger := graylogger{gelf, serviceName, environment}
	return &logger
}

// Info logs a message with the "Info" level (1)
func (gl *graylogger) Info(msg string) {
	gl.logWithLevel(msg, infoLevel)
}

// Error logs a message with the "Error" level (3)
func (gl *graylogger) Error(msg string) {
	gl.logWithLevel(msg, errorLevel)
}

//Debug logs a message with the "Debug" level (0)
func (gl *graylogger) Debug(msg string) {
	gl.logWithLevel(msg, debugLevel)
}

func (gl *graylogger) logWithLevel(msg string, level int) {
	gl.gelf.Log(gl.grayLogMessage(msg, level))
}

// GrayLogMessage represents the json message sent to the log sink
type GrayLogMessage struct {
	Facility     string `json:"facility"`
	Service      string `json:"_service"`
	Environment  string `json:"_environment"`
	ShortMessage string `json:"short_message"`
	Level        string `json:"level"`
}

func (gl *graylogger) grayLogMessage(msg string, level int) string {
	glMsg := GrayLogMessage{
		gl.serviceName,
		gl.serviceName,
		gl.environment,
		msg,
		strconv.Itoa(level),
	}
	b, _ := json.Marshal(glMsg)
	return string(b)
}
