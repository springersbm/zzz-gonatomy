package gonatomy

import (
	"encoding/json"
	"fmt"
	"net/http"
)

// InternalPages gives you handlers for the app anatomy endpoints
type InternalPages struct {
	Status              int      // the status code which is returned on the status page
	emails              []string // email addresses that recieve alerts
	monitoringInterval  int      // how often the monitoring is done
	monitoringThreshold int      // how many times the monitoring has to fail
	revision            string   // the current git revision
	environment         string
	build               string
	configEntries       map[string]string
}

// NewInternalPages creates a new instance of InternalPages
func NewInternalPages(emails []string, monitoringInterval int, monitoringThreshold int, revision string, environment string, build string, configEntries map[string]string) *InternalPages {
	s := new(InternalPages)
	s.Status = http.StatusOK
	s.emails = emails
	s.monitoringInterval = monitoringInterval
	s.monitoringThreshold = monitoringThreshold
	s.environment = environment
	s.build = build
	s.configEntries = configEntries
	s.revision = revision
	return s
}

const indexPageBody = `
	{
		"/config": "runtime configuration",
		"/status": "reachability",
		"/version": "identity of current build version"
	}
`

const jsonError = "Could not serialise page's data into JSON"

type statusPageData struct {
	Alerting   alertingData   `json:"alerting"`
	Monitoring monitoringData `json:"monitoring"`
}

type alertingData struct {
	Emails []string `json:"emails"`
}

type monitoringData struct {
	Interval  int `json:"interval"`
	Threshold int `json:"threshold"`
}

// InternalHandler routes requests for /internal to the appropiate handles
func (s *InternalPages) InternalHandler(w http.ResponseWriter, r *http.Request) {

	switch r.URL.Path {
	case "/internal", "/internal/":
		ServeInternalHomePage(w, r)
	case "/internal/status":
		s.StatusHandler(w, r)
	case "/internal/version":
		s.RenderVersionPage(w, r)
	case "/internal/config":
		s.ConfigHandler(w, r)
	}
}

// StatusHandler is a function that can handle http requests
func (s *InternalPages) StatusHandler(w http.ResponseWriter, r *http.Request) {
	status := statusPageData{alertingData{s.emails}, monitoringData{s.monitoringInterval, s.monitoringThreshold}}
	renderJSONAndWriteResponse(status, w, s.Status)
}

// RenderVersionPage renders the git revision of the currently deployed server
func (s *InternalPages) RenderVersionPage(w http.ResponseWriter, r *http.Request) {
	pageData := map[string]interface{}{
		"revision": s.revision,
	}
	renderJSONAndWriteResponse(pageData, w, http.StatusOK)
}

// ServeInternalHomePage gives you a http.HandleFunc to serve the app anatomy standard homepage, which lists internal endpoints
func ServeInternalHomePage(w http.ResponseWriter, r *http.Request) {
	setContentTypeJSON(w)
	fmt.Fprint(w, indexPageBody)
}

func renderJSONAndWriteResponse(data interface{}, w http.ResponseWriter, status int) {
	json, err := json.MarshalIndent(data, "", "  ")

	if err != nil {
		http.Error(w, jsonError, http.StatusInternalServerError)
	} else {
		setContentTypeJSON(w)
		w.WriteHeader(status)
		fmt.Fprint(w, string(json))
	}
}

func setContentTypeJSON(w http.ResponseWriter) {
	w.Header().Set("Content-Type", "application/json")
}
