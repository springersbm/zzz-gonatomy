package gonatomy

import (
	"github.com/quipo/statsd"
)

func newStatsDTimer(conf statsConfig, serviceName string) StatsReporter {
	statsdAddress := conf.StatsDServer() + ":" + conf.StatsDPort()

	prefix := "services." + serviceName + "." + conf.Environment() + "."
	statsdclient := statsd.NewStatsdClient(statsdAddress, prefix)
	statsdclient.CreateSocket()
	return statsdclient
}
