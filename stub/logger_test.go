package stub

import (
	"testing"
)

func Test_it_records_log_messages(t *testing.T) {
	logger := NewLogger(false)
	logger.Info("foo")
	logger.Info("bar")

	if logger.InfoMessages[0] != "foo" {
		t.Error("Didnt record foo info message")
		t.Fatal()
	}

	if logger.InfoMessages[1] != "bar" {
		t.Error("Didnt record bar info message")
		t.Fatal()
	}
}
