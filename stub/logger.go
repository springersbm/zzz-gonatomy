package stub

import (
	"fmt"
)

// Logger is used for when you wish to test what you are logging.
type Logger struct {
	noisy bool

	// Records all info messages sent
	InfoMessages []string

	// Records all error messages sent
	ErrorMessages []string

	// Records all debug messages sent
	DebugMessages []string
}

// NewLogger returns a new stub logger, the noisy params determines whether calls to the logging functons will be printed or not
func NewLogger(noisy bool) *Logger {
	l := new(Logger)
	l.noisy = noisy
	return l
}

// Info is a stubbed version of the Info log function. Records all calls into the InfoMessages array
func (s *Logger) Info(msg string) {
	s.InfoMessages = append(s.InfoMessages, msg)
	s.shout(msg)
}

// Error is a stubbed version of the Error log function. Records all calls into the ErrorMessages array
func (s *Logger) Error(msg string) {
	s.InfoMessages = append(s.ErrorMessages, msg)
	s.shout(msg)
}

// Debug is a stubbed version of the Debug log function. Records all calls into the DebugMessages array
func (s *Logger) Debug(msg string) {
	s.InfoMessages = append(s.DebugMessages, msg)
	s.shout(msg)
}

func (s *Logger) shout(msg string) {
	if s.noisy {
		fmt.Println(msg)
	}
}
