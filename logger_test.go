package gonatomy

import (
	"testing"
)

func Test_it_returns_stdout_logger_when_local(t *testing.T) {
	logger := NewLogger(123, "foo", "bar", environmentLocal)
	_, isStdout := logger.(*stdOutLogger)

	if !isStdout {
		t.Fatal("Logger returned from constructor was not a stdout logger")
	}
}

func Test_it_returns_graylogger_when_not_local(t *testing.T) {
	logger := NewLogger(123, "foo", "bar", "not"+environmentLocal)
	_, isGray := logger.(*graylogger)

	if !isGray {
		t.Fatal("Logger returned from constructor was not a graylogger")
	}
}
