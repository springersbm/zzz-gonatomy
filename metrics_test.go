package gonatomy

import (
	"github.com/quipo/statsd"
	"testing"
)

type fakeConfig struct {
	env string
}

func (f *fakeConfig) StatsDServer() string { return "x" }
func (f *fakeConfig) StatsDPort() string   { return "x" }
func (f *fakeConfig) Environment() string  { return f.env }

func Test_it_returns_a_stdouttimer_when_local(t *testing.T) {
	fakeConfig := &fakeConfig{environmentLocal}
	timer := NewStatsReporter(fakeConfig, testServiceName)

	_, isStdout := timer.(*stdOutTimer)

	if !isStdout {
		t.Fatal("Timer was not a stdout timer")
	}
}

func Test_it_returns_a_statsdtimer_when_not_local(t *testing.T) {
	fakeConfig := &fakeConfig{"not local"}
	timer := NewStatsReporter(fakeConfig, testServiceName)

	_, isStdout := timer.(*statsd.StatsdClient)

	if !isStdout {
		t.Fatal("Timer was not a statsd timer")
	}
}
