package gonatomy

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"
)

const testThresholds = 3
const testRevision = "weofijwoeifjweoifjwoij12323oij"
const testEnv = "local"
const testBuild = "9000"

func TestItReturns200ForStatusPage(t *testing.T) {
	handler := NewInternalPages(testEmails(), testThresholds, testThresholds, testRevision, testEnv, testBuild, nil)
	req, _ := http.NewRequest("GET", "/internal/status", nil)

	w := httptest.NewRecorder()
	handler.InternalHandler(w, req)

	checkItWasOK(w, t)

	expectedStatus := statusPageData{alertingData{testEmails()}, monitoringData{testThresholds, testThresholds}}

	checkContentType(w, t)

	var actualStatus statusPageData
	err := json.Unmarshal(w.Body.Bytes(), &actualStatus)

	if err != nil {
		t.Errorf("Couldn't parse JSON from status page", err)
	}

	if expectedStatus.Alerting.Emails[0] != testEmails()[0] {
		t.Error("The returned JSON doesn't appear to be correct")
	}
}

func TestItReturnsDifferentStatusCodesWhenSet(t *testing.T) {
	handler := NewInternalPages(testEmails(), testThresholds, testThresholds, testRevision, testEnv, testBuild, nil)
	handler.Status = http.StatusInternalServerError
	req, _ := http.NewRequest("GET", "/internal/status", nil)

	w := httptest.NewRecorder()
	handler.StatusHandler(w, req)

	if w.Code != http.StatusInternalServerError {
		t.Fatalf("Unexpected status, expected 503 but got %d", w.Code)
	}

	checkContentType(w, t)
}

func TestItReturnsAVersion(t *testing.T) {
	handler := NewInternalPages(testEmails(), testThresholds, testThresholds, testRevision, testEnv, testBuild, nil)
	req, _ := http.NewRequest("GET", "/internal/version", nil)

	w := httptest.NewRecorder()
	handler.InternalHandler(w, req)

	checkItWasOK(w, t)
	checkContentType(w, t)

	var versionJSON map[string]string
	err := json.Unmarshal(w.Body.Bytes(), &versionJSON)

	if err != nil {
		t.Fatalf("It looks like the version page didnt return valid json")
	}

	if versionJSON["revision"] != testRevision {
		t.Errorf("The expected version was not returned, expected %s got %s", testRevision, versionJSON["revision"])
	}
}

func TestItReturnsIndexPage(t *testing.T) {
	handler := NewInternalPages(testEmails(), testThresholds, testThresholds, testRevision, testEnv, testBuild, nil)
	req, _ := http.NewRequest("GET", "/internal", nil)

	w := httptest.NewRecorder()
	handler.InternalHandler(w, req)

	checkItWasOK(w, t)

	if w.Body.String() != indexPageBody {
		t.Error("Didnt get the index page body")
	}

	checkContentType(w, t)
}

func TestItReturnsConfig(t *testing.T) {
	testConfig := make(map[string]string)
	testConfig["Chris"] = "James"

	handler := NewInternalPages(testEmails(), testThresholds, testThresholds, testRevision, testEnv, testBuild, testConfig)
	req, _ := http.NewRequest("GET", "/internal/config", nil)

	w := httptest.NewRecorder()
	handler.InternalHandler(w, req)

	checkItWasOK(w, t)
	checkContentType(w, t)

	var config configData
	err := json.Unmarshal(w.Body.Bytes(), &config)

	if err != nil {
		t.Fatalf("Couldn't parse config response body into configData struct", w.Body.String())
	}

	if config.Build != testBuild {
		t.Error("Build version was not returned")
	}

	if config.Env != testEnv {
		t.Error("Environment was not returned")
	}

	if config.Config["Chris"] != "James" {
		t.Error("Environment variables config part was not returned")
	}
}

func TestJSONRendererFailsGravefully(t *testing.T) {
	data := func() {

	}
	w := httptest.NewRecorder()
	renderJSONAndWriteResponse(data, w, http.StatusOK)

	if w.Code != http.StatusInternalServerError {
		t.Errorf("Trying to render bad JSON should've resulted in a 500, but got a %d", w.Code)
	}
}

func testEmails() []string {
	return []string{"foo@baz.com"}
}

func checkContentType(w http.ResponseWriter, t *testing.T) {
	if w.Header().Get("Content-type") != "application/json" {
		t.Fatalf("Expected application/json content type %s", w.Header().Get("Content-Type"))
	}
}

func checkItWasOK(w *httptest.ResponseRecorder, t *testing.T) {
	if w.Code != http.StatusOK {
		t.Fatalf("Unexpected status, expected 200 but got %d", w.Code)
	}
}
