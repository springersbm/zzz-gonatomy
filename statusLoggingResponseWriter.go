package gonatomy

import (
	"net/http"
)

// fyi - this kind of syntax of "including" an interface means is go's way of composition
type statusLoggingResponseWriter struct {
	StatusCode int
	http.ResponseWriter
}

func newStatusLoggingResponseWriter(resWriter http.ResponseWriter) *statusLoggingResponseWriter {
	loggingResponseWriter := new(statusLoggingResponseWriter)
	loggingResponseWriter.ResponseWriter = resWriter
	loggingResponseWriter.StatusCode = http.StatusOK
	return loggingResponseWriter
}

// We can then override the write header thing, which gives us access to the status code
func (w *statusLoggingResponseWriter) WriteHeader(code int) {
	w.StatusCode = code
	w.ResponseWriter.WriteHeader(code)
}
