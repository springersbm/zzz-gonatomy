package gonatomy

import (
	"os"
	"strings"
)

func getEnvironmentVaribles() map[string]string {
	env := make(map[string]string)

	for _, e := range os.Environ() {
		parts := strings.Split(e, "=")
		env[parts[0]] = parts[1]
	}

	return env
}
