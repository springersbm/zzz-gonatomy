package gonatomy

import (
	"log"
)

type stdOutLogger struct{}

func newStdOutLogger() *stdOutLogger {
	return new(stdOutLogger)
}

func (s *stdOutLogger) Info(msg string) {
	log.Println("INFO: " + msg)
}

func (s *stdOutLogger) Error(msg string) {
	log.Println("ERROR:  " + msg)
}

func (s *stdOutLogger) Debug(msg string) {
	log.Println("DEBUG: " + msg)
}
