package gonatomy

// Logger describes the interface for logging to a reporter, such as graylog or stdout
type Logger interface {
	Info(string)
	Error(string)
	Debug(string)
}

const environmentLocal = "local"

// NewLogger will return a logger implementation for you to use based on your environment. For local it will be stdout
func NewLogger(port int, grayLogURL string, serviceName string, environment string) Logger {
	if environment == environmentLocal {
		return newStdOutLogger()
	}
	return newGrayLogger(port, grayLogURL, serviceName, environment)
}
