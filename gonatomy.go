/*
Package gonatomy provides tools for writing microservices in the style of Springer App Anatomy (which is similar to 12 factor app).

Specifically this provides environment specific implementations for logging and metrics and Negroni style middleware to ensure all your HTTP handlers log and measure useful information.
*/
package gonatomy

// NewTools will return you the various NFR tools you need when writing an app anatomy compliant app. Returns an error if it couldn't successfully load all configuration values from the environment
func NewTools(serviceName string) (metrics StatsReporter, logger Logger, port string, internalPages *InternalPages, err error) {
	conf, err := newConfig()

	if err != nil {
		return
	}

	logger = NewLogger(conf.GelfPort(), conf.GelfHost(), serviceName, conf.Environment())
	metrics = NewStatsReporter(conf, serviceName)
	port = conf.Port()

	internalPages = NewInternalPages(
		conf.SupportEmails(),
		conf.MonitoringInterval(),
		conf.MonitoringThreshold(),
		conf.Revision,
		conf.Environment(),
		conf.Build,
		getEnvironmentVaribles())

	return
}
