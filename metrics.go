package gonatomy

type statsConfig interface {
	StatsDServer() string
	StatsDPort() string
	Environment() string
}

// StatsReporter is the interface for writing metrics
type StatsReporter interface {
	Timing(string, int64) error
	Incr(string, int64) error
	Absolute(string, int64) error
	Close() error
}

// NewStatsReporter will return a metrics implementation based on your environment, if local it will be stdout
func NewStatsReporter(conf statsConfig, serviceName string) StatsReporter {
	if conf.Environment() == "local" {
		return newStdOutTimer()
	}
	return newStatsDTimer(conf, serviceName)
}
