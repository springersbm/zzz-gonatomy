package gonatomy

import (
	"net/http"
)

// ConfigHandler renders the standard config page
func (i *InternalPages) ConfigHandler(w http.ResponseWriter, r *http.Request) {
	data := configData{i.build, i.configEntries, i.environment}
	renderJSONAndWriteResponse(data, w, http.StatusOK)
}

type configData struct {
	Build  string
	Config map[string]string
	Env    string
}
