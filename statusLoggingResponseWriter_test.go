package gonatomy

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"
)

func Test_it_logs_status_codes_when_written(t *testing.T) {
	handler := func(w http.ResponseWriter, r *http.Request) {
		http.Error(w, "something failed", http.StatusInternalServerError)
	}

	req, _ := http.NewRequest("GET", "http://example.com/foo", nil)
	w := httptest.NewRecorder()

	loggingWriter := newStatusLoggingResponseWriter(w)

	handler(loggingWriter, req)

	if loggingWriter.StatusCode != http.StatusInternalServerError {
		t.Fatalf("Unexpected status code recorded, expected %d but got %d", http.StatusInternalServerError, loggingWriter.StatusCode)
	}
}

func TestItDefaultsTo200ForStatus(t *testing.T) {
	handler := func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprint(w, "Hello, world")
	}

	req, _ := http.NewRequest("GET", "http://example.com/foo", nil)
	w := httptest.NewRecorder()

	loggingWriter := newStatusLoggingResponseWriter(w)

	handler(loggingWriter, req)

	if loggingWriter.StatusCode != http.StatusOK {
		t.Fatalf("Unexpected status code recorded, expected %d but got %d", http.StatusOK, loggingWriter.StatusCode)
	}
}
