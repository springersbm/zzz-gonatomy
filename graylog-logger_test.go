package gonatomy

import (
	"fmt"
	"testing"
)

type StubLogger struct {
	lastMsg string
}

func (s *StubLogger) Log(msg string) {
	s.lastMsg = msg
}

const testMessage = "Hello, world"
const testServiceName = "foo"
const testEnvironment = "bar"

func Test_it_logs_info_with_the_proper_json_at_correct_level(t *testing.T) {
	stubLogger, logger := makeTestLogger()
	logger.Info(testMessage)

	if expected := expectedJSON(infoLevel); expected != stubLogger.lastMsg {
		t.Fatalf("Logged json was not correct expected [%s], got [%s]", expected, stubLogger.lastMsg)
	}
}

func Test_it_logs_errors_with_the_correct_level(t *testing.T) {
	stubLogger, logger := makeTestLogger()
	logger.Error(testMessage)

	if expected := expectedJSON(errorLevel); expected != stubLogger.lastMsg {
		t.Fatalf("Logged json was not correct expected [%s], got [%s]", expected, stubLogger.lastMsg)
	}
}

func Test_it_logs_debug_with_the_correct_level(t *testing.T) {
	stubLogger, logger := makeTestLogger()
	logger.Debug(testMessage)

	if expected := expectedJSON(debugLevel); expected != stubLogger.lastMsg {
		t.Fatalf("Logged json was not correct expected [%s], got [%s]", expected, stubLogger.lastMsg)
	}
}

const grayLogMessageTemplate = `{"facility":"%s","_service":"%s","_environment":"%s","short_message":"%s","level":"%d"}`

func expectedJSON(level int) string {
	return fmt.Sprintf(grayLogMessageTemplate, testServiceName, testServiceName, testEnvironment, testMessage, level)
}

func makeTestLogger() (*StubLogger, *graylogger) {
	stubLogger := &StubLogger{}
	return stubLogger, &graylogger{stubLogger, testServiceName, testEnvironment}
}
