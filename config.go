package gonatomy

import (
	"fmt"
	"os"
	"strconv"
	"strings"
)

type environmentConfig struct {
	conf     map[string]string
	Build    string
	Revision string
}

func newConfig() (*environmentConfig, error) {
	c := new(environmentConfig)
	c.conf = make(map[string]string)

	c.Build = os.Getenv("BUILD")
	c.Revision = os.Getenv("GIT_REVISION")
	c.conf["PORT"] = os.Getenv("PORT")
	c.conf["STATSD_HOST"] = os.Getenv("STATSD_HOST")
	c.conf["STATSD_PORT"] = os.Getenv("STATSD_PORT")
	c.conf["ENVIRONMENT"] = os.Getenv("ENVIRONMENT")
	c.conf["GELF_HOST"] = os.Getenv("GELF_HOST")
	c.conf["GELF_PORT"] = os.Getenv("GELF_PORT")
	c.conf["SUPPORT_EMAILS"] = os.Getenv("SUPPORT_EMAILS")
	c.conf["MONITORING_INTERVAL"] = os.Getenv("MONITORING_INTERVAL")
	c.conf["MONITORING_THRESHOLD"] = os.Getenv("MONITORING_THRESHOLD")

	emptyFields := c.checkForEmptyFields()
	if len(emptyFields) > 0 {
		return nil, fmt.Errorf("Undefined environment variables: %v", emptyFields)
	}

	if c.MonitoringInterval() == 0 {
		return nil, fmt.Errorf("Monitoring interval was not set correctly [%v]", os.Getenv("MONITORING_INTERVAL"))
	}

	if c.MonitoringThreshold() == 0 {
		return nil, fmt.Errorf("Monitoring threshold was not set correctly [%v]", os.Getenv("MONITORING_THRESH"))
	}

	if c.GelfPort() == 0 {
		return nil, fmt.Errorf("Log port was not set correctly [%v]", os.Getenv("GELF_PORT"))
	}

	return c, nil
}

func (c *environmentConfig) checkForEmptyFields() []string {
	var emptyFields []string
	for key, value := range c.conf {
		if value == "" {
			emptyFields = append(emptyFields, key)
		}
	}
	return emptyFields
}

func (c *environmentConfig) MonitoringThreshold() int {
	i, _ := strconv.Atoi(c.conf["MONITORING_THRESHOLD"])
	return i
}

func (c *environmentConfig) MonitoringInterval() int {
	i, _ := strconv.Atoi(c.conf["MONITORING_INTERVAL"])
	return i
}

func (c *environmentConfig) SupportEmails() []string {
	emails := strings.Split(c.conf["SUPPORT_EMAILS"], ",")

	for i := 0; i < len(emails); i++ {
		emails[i] = strings.TrimSpace(emails[i])
	}

	return emails
}

func (c *environmentConfig) Port() string {
	return c.conf["PORT"]
}

func (c *environmentConfig) StatsDServer() string {
	return c.conf["STATSD_HOST"]
}

func (c *environmentConfig) StatsDPort() string {
	return c.conf["STATSD_PORT"]
}

func (c *environmentConfig) Environment() string {
	return c.conf["ENVIRONMENT"]
}

func (c *environmentConfig) GelfHost() string {
	return c.conf["GELF_HOST"]
}

func (c *environmentConfig) GelfPort() int {
	i, _ := strconv.Atoi(c.conf["GELF_PORT"])
	return i
}
