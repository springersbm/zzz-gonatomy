package gonatomy

import (
	"os"
	"testing"
)

func TestLoadsEnvironmentVariables(t *testing.T) {

	os.Setenv("PORT", "8888")
	os.Setenv("STATSD_HOST", "metricsurl")
	os.Setenv("STATSD_PORT", "metricsport")
	os.Setenv("ENVIRONMENT", "test")
	os.Setenv("GELF_HOST", "logurl")
	os.Setenv("GELF_PORT", "1111")
	os.Setenv("SUPPORT_EMAILS", "chris@springer.com, bob@springer.com")
	os.Setenv("MONITORING_INTERVAL", "60")
	os.Setenv("MONITORING_THRESHOLD", "3")

	config, _ := newConfig()

	if config.MonitoringInterval() != 60 {
		t.Errorf("Monitoring interval not pulled from env %v", config.MonitoringInterval())
	}

	if config.MonitoringThreshold() != 3 {
		t.Errorf("Monitoring threshold not pulled from env [%v]", config.MonitoringThreshold())
	}

	if config.SupportEmails()[0] != "chris@springer.com" || config.SupportEmails()[1] != "bob@springer.com" {
		t.Errorf("Support emails were not correctly pulled from env %v", config.SupportEmails())
	}

	if config.Port() != "8888" {
		t.Error("Port was not correctly pulled from env")
	}

	if config.StatsDServer() != "metricsurl" {
		t.Error("Metrics url was not correctly pulled from env")
	}

	if config.StatsDPort() != "metricsport" {
		t.Error("Metrics port was not correctly pulled from env")
	}

	if config.Environment() != "test" {
		t.Error("Environment name was not correctly pulled from env")
	}

	if config.GelfHost() != "logurl" {
		t.Error("Logsink url not correctly pulled from env")
	}

	if config.GelfPort() != 1111 {
		t.Error("Logsink port not correctly pulled from env")
	}
}

func TestItErrorsIfFieldsAreUndefined(t *testing.T) {
	os.Setenv("PORT", "")
	_, err := newConfig()

	if err == nil {
		t.Error("No env variables were set so an error should've been returned")
	}
}
